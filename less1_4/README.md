Кастомная сборка Nginx с модулем Lua

Для сборки:

```bash
docker build -t juneway-nginx . 
```

Запуск:

```bash
 docker run -d --name juneway-lesson14 -p 8080:80 -v $PWD/nginx.conf:/usr/local/nginx/conf/nginx.conf juneway-nginx
```

Для проверки воспользоваться curl'ом или открыть в браузере

![image-20200804225503507](img/result.png)
