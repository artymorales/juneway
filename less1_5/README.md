## Описание задания

1. Запустить в контейнере strace

2. Запустить сборку имеджа внутри контейнера

Задача на 100 баллов 
Сделать самим образ с докер сервисом внутри на основе дебиана.

## Отчет

1. Поставил strace внутри образа и зпустил

<img src="img/strace.png" alt="strace" style="zoom:50%;" />

2. Запустил сборку имеджа из  Dockerfile урока 1.4.

   ![image-20200806221739930](img/images.png)

3. Приложен Dockerfile

   Для билда:

```bash
docker build -t custom-dind .
```

​	Для запуска:

```
docker run -it --privileged custom-dind:latest
```

